use alienfile;

use File::chdir;

plugin 'Probe::CommandLine' => (
  command => 'lua',
  args    => [ '-v' ],
  match   => qr/^Lua/,
);

configure {
  requires 'File::chdir' => 0;
};

share {
    requires 'Path::Tiny' => 0;

    start_url 'http://luajit.org/download.html';

    plugin Download => (
        filter  => qr/^LuaJIT-\d[.]\d[.]\d\.tar\.gz$/,
        version => qr/^LuaJIT-(\d[.]\d[.]\d)\.tar\.gz$/,
    );

    plugin 'Extract' => 'tar.gz';

    build sub {
        my ($build) = @_;

        my $build_dir = Path::Tiny::path( $build->install_prop->{extract} );
        local $CWD = $build_dir;

        $build->runtime_prop->{command} = 'luajit';

        $build->system('%{make}');

        return windows_install($build) if $^O =~ /MSWin32/;

        my %vars = ( prefix => $build->install_prop->{prefix} );
        unless (File::Which::which('install')) {
            $vars{install_x} = $vars{install_f} = 'cp -p';
        }

        my @vars = map { uc($_) . '=' . $vars{$_} } keys %vars;
        $build->system( '%{make}', @vars, 'install' );
    };
};

gather [
  [ 'pkg-config', '--modversion', 'luajit', \q(%{.runtime.version}) ],
  [ 'pkg-config', '--cflags',     'luajit', \q(%{.runtime.cflags}) ],
  [ 'pkg-config', '--libs',       'luajit', \q(%{.runtime.libs}) ],
];

sub windows_install {
    my ($build) = @_;

    my $tar = Path::Tiny::path( $build->install_prop->{extract} );
    my $tgt = Path::Tiny::path( $build->install_prop->{prefix} );

    my $bin = $tgt->child('bin');
    my $jit = $bin->child('lua/jit');

    $bin->mkpath;
    $jit->mkpath;

    $_->copy( $bin->child( $_->basename ) )
        for $tar->child('src')->children(qr/^lua.*(exe|dll)$/);

    $_->copy( $jit->child( $_->basename ) )
        for $tar->child('src/jit')->children;

    my $srcpc = $tar->child('etc/luajit.pc');
    my $tgtpc = $tgt->child('lib/pkgconfig/luajit.pc');
    $tgtpc->parent->mkpath;

    my $contents = $srcpc->slurp;
    $contents =~ s/^prefix=.*/prefix=$tgt/;
    $srcpc->copy($tgtpc);
    $tgtpc->spew($contents);
}
